import { addPhoto, removeAllPhotoes } from "./redux/slices/photoesSlice";
import {useDispatch} from "react-redux"
type image = {url: string}
const images: Array<image> = [
    {url: "https://thisimagedoesnotexist.com/images/1.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/2.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/3.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/4.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/5.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/6.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/7.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/8.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/9.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/10.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/11.jpeg"},
    {url: "https://thisimagedoesnotexist.com/images/12.jpeg"}
]
export function useReduxInitial(){
    const dispatch = useDispatch()
    return () =>{
    // dispatch(removeAllPhotoes())
    images.map(item => dispatch(addPhoto(item.url)))}
}
export function useClearRedux(){
    const dispatch = useDispatch()
    return () =>{
        console.log("delete...?")
        dispatch(removeAllPhotoes())
    }

}