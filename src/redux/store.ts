import { configureStore } from '@reduxjs/toolkit'
import photoesReduser from "./slices/photoesSlice"

export const store = configureStore({
  reducer: {
    photoes: photoesReduser
  },
})

export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch