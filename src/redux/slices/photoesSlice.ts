import { createSlice, nanoid } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface Photo {
  id: string;
  url: string
}
const initialState = {value:  [] as Array<Photo>};

export const photoesSlice = createSlice({
  name: 'photoes',
  initialState,
  reducers: {
    addPhoto: {
      reducer: (state, action: PayloadAction<Photo>) =>{
        state.value.push(action.payload)
      },
      prepare: ( url: string)=>{
        const id = nanoid();
        return {
          payload: {
            id,
            url
          }
        }
      },
    },
    removePhoto:{
      reducer: (state, action: PayloadAction<{id:string}>) =>{
        state.value = state.value.filter(photo => photo.id != action.payload.id)
      },
      prepare: ( id: string)=>{
        return {
          payload: {id }
        }
      },
    },
    removeAllPhotoes: state => {
      state.value = initialState.value
    }
  }
})

// Action creators are generated for each case reducer function
export const { addPhoto, removeAllPhotoes } = photoesSlice.actions

export default photoesSlice.reducer