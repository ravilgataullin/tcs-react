import "./GalleryItem.css"
import {useDrag, DragPreviewImage} from 'react-dnd'
// let ItemTypes={ololo: "trololo"};
let MyType = "trololo";


function GalleryItem(props:{imgUrl:string}) {
  const [{isDragging, opacity}, dragRef, dragPreview ]:[any, any, any] = useDrag({
    // type: ItemTypes.ololo,
    type: MyType,
    item: {ololo: "trololo"},
    collect: monitor => ({
      isDragging: !!monitor.isDragging(),
      opacity: monitor.isDragging() ? 0.5 : 1
    }),
  });
   
    return (
    <>
       {/* <DragPreviewImage connect = {dragPreview} src={props.imgUrl}/> */}
       <div ref={dragRef} className="image-card" style={{backgroundImage: `url(${props.imgUrl})`, opacity }}>
        {/* <div className="delete_button">Del</div> */}

       </div>
    </>
       
    );
  }
  
export default GalleryItem;