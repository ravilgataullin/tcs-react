import "./GalleryList.css"
import GalleryItem from "./GalleryItem"
import {image} from "./types"
import type  {Photo} from "../../redux/slices/photoesSlice";
import { useSelector, useDispatch } from 'react-redux'
// const images: Array<image> = [
//         {url: "https://thisimagedoesnotexist.com/images/1.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/2.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/3.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/4.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/5.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/6.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/7.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/8.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/9.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/10.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/11.jpeg"},
//         {url: "https://thisimagedoesnotexist.com/images/12.jpeg"}
//     ]


function GalleryList() {
    const photoes = useSelector((state:any) => state.photoes.value)
    console.log(photoes)
    return (
        <ul className="gallery-list">
            {photoes.map((photo:Photo, index:number ) => (
                <li key={index} >
                    <GalleryItem imgUrl={photo.url}/>
                </li>
            ))}
        </ul>
       
    );
  }
  
export default GalleryList;