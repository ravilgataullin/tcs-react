import{MenuSection as Section} from "./gallery"
function MenuButton(props: {handler:any, selected:boolean, children:any}){
    return (
        <button 
            onClick={props.handler} 
            className={props.selected?"selected":""}
        >
                {props.children}
        </button>
       
    );
}
  
export default MenuButton;