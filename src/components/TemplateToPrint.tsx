import './TemplateToPrint.css';

function TemplateToPrint() {
    return (
    <div className="wrapper">
        <div className="template">
            <div className="image-section">One</div>
            <div className="image-section">Two</div>
            <div className="image-section">Three</div>
            <div className="image-section">Four</div>
            <div className="image-section">Five</div>
            <div className="image-section"> Six</div>
        </div>
        <div className="arrows">
            <div className="arrows-wrapper">

                <div className="arrow left"> {"<"} </div>
                <div className="arrow right"> {">"} </div>
            </div>
        </div>


    </div>
    );
  }
  
export default TemplateToPrint;