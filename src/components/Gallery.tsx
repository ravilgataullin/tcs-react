import "./Gallery.css"
import GalleryList from "./Gallery/GalleryList";
import MenuButton from "./Gallery/MenuButton";
import {useState} from "react";
import {MenuSection as Section} from "./Gallery/gallery"
import { useReduxInitial , useClearRedux} from "../helpers";



function Gallery() {
    const [sectionState, setSectionState] = useState(Section.Tmp)
    
    const renderSection = () => {
        switch(sectionState){
            case Section.Gal: return <GalleryList/>
            case Section.Tmp: return <div> Templates</div>
            case Section.Bg: return <div> Background</div>
        }
    }
    const select = (state: Section) => 
        () => setSectionState(state)
    
    const handler =  useReduxInitial()
    const handler2 = useClearRedux()
    
    return (
    <div className="gallery">
        <button onClick={handler}>Загрузить</button>
        <button onClick={handler2}>Удалить всё</button>

        <ul className="menu">
            <MenuButton handler={select(Section.Gal)} selected={sectionState===Section.Gal}> Галерея</MenuButton>
            <MenuButton handler={select(Section.Tmp)} selected={sectionState===Section.Tmp}> Шаблоны</MenuButton>
            <MenuButton handler={select(Section.Bg)} selected={sectionState===Section.Bg}> Фон</MenuButton>
          
        </ul>
            {  renderSection()  }
            
        
    </div>
    );
  }
  
export default Gallery;