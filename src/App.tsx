import './App.css';
import Header from "./components/Header";
import TemplateToPrint from './components/TemplateToPrint';
import Gallery from './components/Gallery';
import {useReduxInitial} from "./helpers"
import { useRef } from 'react';

function App() {
  // useReduxInitial();
  useRef(useReduxInitial())

  return (
    <div className="App">
      <Header/>
      <div className="content">
        <TemplateToPrint/>
        <Gallery />

      </div>
    </div>
  );
}

export default App;
